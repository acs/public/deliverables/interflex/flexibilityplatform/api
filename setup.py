import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="interflexAPI",
    version="0.0.1",
    author="Jonas Baude",
    author_email="jonas.baude@eonerc.rwth-aachen.de",
    description="InterflexAPI reference implementation",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.rwth-aachen.de/TODO",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
    ],
)
