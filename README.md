# General

This repository provides an open-source Python reference implementation of the InterFlex API specified in [Deliverable 3.](https://interflex-h2020.com/wp-content/uploads/2019/11/D3.4-Interoperable-APIs-Specification_RWTH_InterFlex-v1.0.pdf).
The additional [examples](https://git.rwth-aachen.de/acs/public/deliverables/interflex/flexibilityplatform/d35) repository contain a set of examples and integration test to understand the usage of the API or to varify the functinoality of the API against the specification.

## Copyright

2019, Institute for Automation of Complex Power Systems, EONERC, RWTH Aachen University

## License

This project is released under the terms of the [GPL version 3](COPYING.md).

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

For other licensing options please consult [Prof. Antonello Monti](mailto:amonti@eonerc.rwth-aachen.de).

## Contact

[![EONERC ACS Logo](doc/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- Jonas Baude <jonas.baude@eonerc.rwth-aachen.de>
- Amir Ahmadifar <ahmadifar@eonerc.rwth-aachen.de>

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)
