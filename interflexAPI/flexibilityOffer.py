import json
import requests

def get_offer_id(host, port, entity_id, access_token, version="v1"):

    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/aggregators/" + entity_id + "/flexibilityOffers/offerId"
    headers = {"X-auth-token": "%s" %access_token}
    response = requests.get(url,headers=headers)
    return response.text

def post_offer(host, port, entity_id, access_token, offer, version="v1"):

    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/aggregators/" + entity_id + "/flexibilityOffers"
    headers = {"X-auth-token": "%s" %access_token}
    response = requests.post(url,headers=headers,json=offer)
    print(response.text)
    return response.text

def get_all_offers(host, port, access_token, version="v1"):

    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/aggregators/flexibilityOffers"
    headers = {"X-auth-token": "%s" %access_token}
    response = requests.get(url,headers=headers)
    return response.text

def delete_offer(host, port, entity_id, access_token, offer_id, version="v1"):

    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/aggregators/" + entity_id + "/flexibilityOffers/" + offer_id
    headers = {"X-auth-token": "%s" %access_token}
    response = requests.delete(url,headers=headers)
    return response.text

def create_offer(offer_id, entity_id ,timestamp, deadline, region_id, flexible_power, granularity, duration, unit, activation_time, flex_price, flex_type, currency):

    flex_offer = {
        "id": offer_id,
        "type": "flexibilityOffer",

        "entity_id": {
            "value": entity_id,
            "type" : "String"
        },

        "timestamp": {
            "value": timestamp,
            "type": "DateTime"
        },
        "valid_until": {
            "value": deadline,
            "type": "DateTime"
        },
        "region_id": {
            "value": region_id,
            "type": "String"
        },

        #flexibility
        "flexible_power": {
            "value": flexible_power,
            "type": "Double"
        },
        "granularity": {
            "value": granularity,
            "type": "Double"
        },
        "duration": {
            "value": duration,
            "type": "Double"
        },
        "unit": {
            "value": unit,
            "type": "String"
        },
        "activation_time": {
            "value": activation_time,
            "type": "String"
        },

        #price
        "flex_price": {
            "value": flex_price,
            "type": "String"
        },
        "flex_type": {
            "value": flex_type,
            "type": "String"
        },
        "currency": {
            "value": currency,
            "type": "String"
        }
    }

    return flex_offer