import json
import requests

def post_activation_nack(host, port, aggregator, access_token, activation_nack, version="v1"):

    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/aggregators/" + aggregator + "/flexibilityActivations/nacks"
    headers = {"X-auth-token": "%s" %access_token}
    response = requests.post(url,headers=headers,json=activation_nack)
    return response.text

def get_all_activation_nacks(host, port, access_token, aggregator, version="v1"):

    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/aggregators/" + aggregator + "/flexibilityActivations/nacks"
    headers = {"X-auth-token": "%s" %access_token}
    response = requests.get(url,headers=headers)
    return response.text

def delete_activation_nack(host, port, access_token, aggregator, activation_nack_id, version="v1"):

    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/aggregators/" + aggregator + "/flexibilityActivations/nacks/" + activation_nack_id
    headers = {"X-auth-token": "%s" %access_token}
    response = requests.delete(url,headers=headers)
    return response.text

def create_flex_activation_nack(nack_id, timestamp, offer_id, flexible_power, granularity, duration, unit, activation_time):

    flex_activation_nack = {
        "id": nack_id,
        "type": "flexibilityActivationNack",

        "timestamp": {
            "value": timestamp,
            "type": "DateTime"
        },
        "offer_id": {
            "value": offer_id,
            "type": "String"
        },
    
        #flexibility
        "flexible_power": {
            "value": flexible_power,
            "type": "Double"
        },
        "granularity": {
            "value": granularity,
            "type": "Double"
        },
        "duration": {
            "value": duration,
            "type": "Double"
        },
        "unit": {
            "value": unit,
            "type": "String"
        },
        "activation_time": {
            "value": activation_time,
            "type": "String"
        },

    }

    return flex_activation_nack