import json
import requests

def get_activation_id(host, port, entity_id, access_token, version="v1"):

    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/dsos/" + entity_id + "/flexibilityActivations/activationId"
    headers = {"X-auth-token": "%s" %access_token}
    response = requests.get(url,headers=headers)
    return response.text

def post_activation(host, port, entity_id, aggregator, access_token, flexibility_activation, version="v1"):

    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/dsos/" + entity_id + "/flexibilityActivations/" + aggregator
    headers = {"X-auth-token": "%s" %access_token}
    response = requests.post(url,headers=headers,json=flexibility_activation)
    return response.text

def get_all_activations(host, port, entity_id, access_token, aggregator, version="v1"):

    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/dsos/" + entity_id + "/flexibilityActivations/" + aggregator
    headers = {"X-auth-token": "%s" %access_token}
    response = requests.get(url,headers=headers)
    return response.text

def delete_activation(host, port, entity_id, access_token, aggregator, activation_id, version="v1"):
    
    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/dsos/" + entity_id +  "/flexibilityActivations/" + aggregator + "/" + activation_id
    headers = {"X-auth-token": "%s" %access_token}
    response = requests.delete(url,headers=headers)
    return response.text

def create_activation(activation_id, timestamp, offer_id, flexible_power, granularity, duration, unit, activation_time, flex_price, flex_type, currency):

    flex_activation = {
        "id": activation_id,
        "type": "flexibilityActivation",

        "timestamp": {
            "value": timestamp,
            "type": "DateTime"
        },
        "offer_id": {
            "value": offer_id,
            "type": "String"
        },
        
        #flexibility
        "flexible_power": {
            "value": flexible_power,
            "type": "Double"
        },
        "granularity": {
            "value": granularity,
            "type": "Double"
        },
        "duration": {
            "value": duration,
            "type": "Double"
        },
        "unit": {
            "value": unit,
            "type": "String"
        },
        "activation_time": {                    # TODO needs to be added!
            "value": activation_time,
            "type": "String"
        },

        #price
        "flex_price": {
            "value": flex_price,
            "type": "String"
        },
        "flex_type": {
            "value": flex_type,
            "type": "String"
        },
        "currency": {
            "value": currency,
            "type": "String"
        }
    }

    return flex_activation