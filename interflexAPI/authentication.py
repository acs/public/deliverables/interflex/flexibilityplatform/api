import base64
import json
import requests

def authenticate(host, port, username, user_secret, client_id, client_secret, version="v1"):
    """ Authenticate agains cloud platform in order to obtain an access token and a refresh token
        :param host: hostname or ip adress
        :param port: related port
        :param username: username of the user to be authenticated
        :param user_secret: password of the user
        :param client_id: id of the application at the oauth2 server
        :param client_secret: secret of the application for the oauth2 server
        :optional param version: version of api to be used
        :return: json containing access_token and refresh_token
    """
    
    auth_header = base64.b64encode(str.encode(client_id + ":" + client_secret))
    headers = {"Content-Type": "application/x-www-form-urlencoded", "Authentication": "Basic %s" % str(auth_header, 'utf-8')}
    payload = str.encode("username=" + username + "&password=" + user_secret + "&client_id=" + client_id + "&client_secret=" + client_secret + "&grant_type=password")
    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/authentication/token"

    response = requests.post(url, payload, headers=headers) #, payload, headers=headers)

    if response.status_code == 200:
        return response.text

def refresh(host, port, refresh_token, client_id, client_secret, version="v1"):
    """ Obtain a new access token by using the refresh token
        :param host: hostname or ip adress
        :param port: related port
        :param refresh_token: previously obtained refresh token
        :param client_id: id of the application at the oauth2 server
        :param client_secret: secret of the application for the oauth2 server
        :optional param version: version of api to be used
        :return: json containing access_token and refresh_token
    """

    auth_header = base64.b64encode(str.encode(client_id + ":" + client_secret))
    headers = {"Content-Type": "application/x-www-form-urlencoded", "Authentication": "Basic %s" % str(auth_header, 'utf-8')}
    payload = str.encode("grant_type=refresh_token&refresh_token=" + refresh_token + "&client_id=" + client_id + "&client_secret=" + client_secret)
    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/authentication/refresh"

    response = requests.post(url, payload, headers=headers)

    if response.status_code == 200:
        return response.text