import json
import requests

def get_request_id(host, port, entity_id, access_token, version="v1"):

    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/dsos/" + entity_id + "/flexibilityRequests/requestId"
    headers = {"X-auth-token": "%s" %access_token}
    response = requests.get(url,headers=headers)
    return response.text

def post_request(host, port, entity_id, access_token, request, version="v1"):

    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/dsos/" + entity_id + "/flexibilityRequests"
    headers = {"X-auth-token": "%s" %access_token}
    response = requests.post(url,headers=headers,json=request)
    return response.text

def get_all_requests(host, port, entity_id, access_token, version="v1"):

    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/dsos/" + entity_id + "/flexibilityRequests"
    headers = {"X-auth-token": "%s" %access_token}
    response = requests.get(url,headers=headers)
    return response.text

def delete_request(host, port, entity_id, access_token, request_id, version="v1"):

    url = "http://" + host + ":" + port + "/interflex/api/" + version + "/dsos/" + entity_id + "/flexibilityRequests/" + request_id
    headers = {"X-auth-token": "%s" %access_token}
    response = requests.delete(url,headers=headers)
    return response.text

def create_request(request_id, timestamp, deadline, region_id, flexible_power, granularity, duration, unit):

    flexReq = {
        "id": request_id,
        "type": "flexibilityRequest",

        "timestamp": {
            "value": timestamp,
            "type": "DateTime"
        },
        "offer_deadline": {
            "value": deadline,
            "type": "DateTime"
        },
        "region_id": {
            "value": region_id,
            "type": "String"
        },

        #flexibility
        "flexible_power": {
            "value": flexible_power,
            "type": "Double"
        },
        "granularity": {
            "value": granularity,
            "type": "Double"
        },
        "duration": {
            "value": duration,
            "type": "Double"
        },
        "unit": {
            "value" : unit,
            "type": "String"
        }
    }

    return flexReq
